vVoteThirdParty
===============

This is intended as a meta-package that contains links or copies of third party libraries that the vVote system depends on. It is not intended that there are any changes in the packages, however, there are currently minor changes to fix performance issues in the Web Server package. Details below.


Changes
=======
NanoHTTPD.java has been modified to change how it reads parameters. The original method looked for blocks ending in newline characters, however, this caused a significant bottleneck when sending large quantities of data in the parameters, for example, during the print function on the Android device. We have modified it to use a BufferedReader.readLine which has significantly improved performance. Once we have fully analysed why the original method was slow we will look to contribute back a fix to the original project and hopefully remove the need for the modification
